﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

public class AttractSystem : ComponentSystem
{
    public float3 center;
    public float maxDistanceSqrd;
    public float strength;


    protected override void OnUpdate()
    {
        Entities.ForEach(
             (ref PhysicsVelocity velocity,
               ref Translation position,
               ref Rotation rotation) =>
             {
                 float3 diff = center - position.Value;
                 float distSqrd = math.lengthsq(diff);
                 if (distSqrd < maxDistanceSqrd)
                 {
                     // Alter linear velocity
                     velocity.Linear += strength * (diff / math.sqrt(distSqrd));
                 }
             });

    }

    public static void ApplyImpulse(ref PhysicsVelocity pv, PhysicsMass pm,
       Translation t, Rotation r, float3 impulse, float3 point)
    {
        // Linear
        pv.Linear += impulse;

        // Angular
        {
            // Calculate point impulse
            var worldFromEntity = new RigidTransform(r.Value, t.Value);
            var worldFromMotion = math.mul(worldFromEntity, pm.Transform);
            float3 angularImpulseWorldSpace = math.cross(point - worldFromMotion.pos, impulse);
            float3 angularImpulseInertiaSpace = math.rotate(math.inverse(worldFromMotion.rot), angularImpulseWorldSpace);

            pv.Angular += angularImpulseInertiaSpace * pm.InverseInertia;
        }
    }
}

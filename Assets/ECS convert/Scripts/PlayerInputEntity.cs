﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

public struct PlayerInputComponent : IComponentData
{
    public float3 input;
}

public class PlayerInputSystem : JobComponentSystem
{
    public struct PlayerInputJob : IJobForEach<PlayerInputComponent>
    {
        public float3 input;
        public void Execute(ref PlayerInputComponent input)
        {
            input.input = this.input;
        }
    }

    protected unsafe override JobHandle OnUpdate(JobHandle inputDeps)
    {
        Vector3 _input = new Vector2(CrossPlatformInputManager.GetAxisRaw("Horizontal"),
            CrossPlatformInputManager.GetAxisRaw("Vertical"));

        var job = new PlayerInputJob { input = _input };
        return job.Schedule(this, inputDeps);
    }
}

public class PlayerInputEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    // implement IConverGameObjectToEntity
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new PlayerInputComponent() { });
    }
}
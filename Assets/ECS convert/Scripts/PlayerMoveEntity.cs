﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

public struct PlayerMoveComponent : IComponentData
{
    public float speed;
    public float2 vectorSpeed;
    public float3 MovementVector;
    public float RotationAngle;

}
[UpdateBefore(typeof(BuildPhysicsWorld))]
public class PlayerMoveSystem : ComponentSystem
{
    protected unsafe override void OnUpdate()
    {
        Entities.ForEach((ref PlayerMoveComponent playerMoveComponent,
            ref PhysicsCollider collider,
            ref PhysicsVelocity velocity,
            ref Translation position,
            ref Rotation rotation) =>
        {

            ref PhysicsWorld world = ref World.Active.GetExistingSystem<BuildPhysicsWorld>().PhysicsWorld;
            // Movement
            {
                float x = Input.GetAxis("Horizontal");
                float z = Input.GetAxis("Vertical");


                bool haveInput = Mathf.Abs(x) > Mathf.Epsilon || Mathf.Abs(z) > Mathf.Epsilon;
                if (!haveInput)
                {
                    playerMoveComponent.MovementVector = float3.zero;
                }
                else
                {
                    float3 movement = math.rotate(quaternion.RotateY(playerMoveComponent.RotationAngle), new float3(x, 0, z));
                    playerMoveComponent.MovementVector = math.normalize(movement);
                }
            }
            float3 lv = velocity.Linear;

            lv = playerMoveComponent.MovementVector * playerMoveComponent.speed;

            velocity.Linear = lv;
        });
    }
}

public class PlayerMoveEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new PlayerMoveComponent()
        {
            speed = 5,
            vectorSpeed = float2.zero,
            MovementVector = float3.zero,
            RotationAngle = 0

        });
    }
}
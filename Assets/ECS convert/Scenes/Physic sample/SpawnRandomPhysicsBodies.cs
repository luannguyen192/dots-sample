﻿using System;
using Unity.Physics;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using UnityEngine;
using Unity.Transforms;
using Collider = Unity.Physics.Collider;
using Unity.Rendering;
using UnityEngine.SceneManagement;


public class SpawnRandomPhysicsBodies : MonoBehaviour
{
    public GameObject prefab;
    public float3 range;
    public int count;

    void OnEnable() { }

    public static void RandomPointsOnCircle(float3 center, float3 range, ref NativeArray<float3> positions, ref NativeArray<quaternion> rotations)
    {
        var count = positions.Length;
        // initialize the seed of the random number generator 
        Unity.Mathematics.Random random = new Unity.Mathematics.Random();
        random.InitState(10);
        for (int i = 0; i < count; i++)
        {
            positions[i] = center + random.NextFloat3(-range, range);
            rotations[i] = random.NextQuaternionRotation();
        }
    }

    void Start()
    {
        if (!enabled) return;

        // Create entity prefab from the game object hierarchy once
     //   Entity sourceEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, World.Active);
        Entity sourceEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, World.Active);

        var entityManager = World.Active.EntityManager;

        var positions = new NativeArray<float3>(count, Allocator.Temp);
        var rotations = new NativeArray<quaternion>(count, Allocator.Temp);
        RandomPointsOnCircle(transform.position, range, ref positions, ref rotations);

        BlobAssetReference<Collider> sourceCollider = entityManager.GetComponentData<PhysicsCollider>(sourceEntity).Value;
        for (int i = 0; i < count; i++)
        {
            var instance = entityManager.Instantiate(sourceEntity);
            entityManager.SetComponentData(instance, new Translation { Value = positions[i] });
            entityManager.SetComponentData(instance, new Rotation { Value = rotations[i] });
            entityManager.SetComponentData(instance, new PhysicsCollider { Value = sourceCollider });
        }

        positions.Dispose();
        rotations.Dispose();
    }

    public Entity CreateDynamicSphere(RenderMesh displayMesh, float radius, float3 position, quaternion orientation)
    {
        // Sphere with default filter and material. Add to Create() call if you want non default:
        BlobAssetReference<Unity.Physics.Collider> spCollider = Unity.Physics.SphereCollider.Create(float3.zero, radius);
        return CreateBody(displayMesh, position, orientation, spCollider, float3.zero, float3.zero, 1.0f, true);
    }

    public unsafe Entity CreateBody(RenderMesh displayMesh, float3 position, quaternion orientation,
        BlobAssetReference<Collider> collider,
        float3 linearVelocity, float3 angularVelocity, float mass, bool isDynamic)
    {
        EntityManager entityManager = World.Active.EntityManager;
        ComponentType[] componentTypes = new ComponentType[isDynamic ? 7 : 4];

        componentTypes[0] = typeof(RenderMesh);
        componentTypes[1] = typeof(TranslationProxy);
        componentTypes[2] = typeof(RotationProxy);
        componentTypes[3] = typeof(PhysicsCollider);
        if (isDynamic)
        {
            componentTypes[4] = typeof(PhysicsVelocity);
            componentTypes[5] = typeof(PhysicsMass);
            componentTypes[6] = typeof(PhysicsDamping);
        }
        Entity entity = entityManager.CreateEntity(componentTypes);

        entityManager.SetSharedComponentData(entity, displayMesh);

        entityManager.AddComponentData(entity, new Translation { Value = position });
        entityManager.AddComponentData(entity, new Rotation { Value = orientation });
        entityManager.SetComponentData(entity, new PhysicsCollider { Value = collider });
        if (isDynamic)
        {
            Collider* colliderPtr = (Collider*)collider.GetUnsafePtr();
            entityManager.SetComponentData(entity, PhysicsMass.CreateDynamic(colliderPtr->MassProperties, mass));

            float3 angularVelocityLocal = math.mul(math.inverse(colliderPtr->MassProperties.MassDistribution.Transform.rot), angularVelocity);
            entityManager.SetComponentData(entity, new PhysicsVelocity()
            {
                Linear = linearVelocity,
                Angular = angularVelocityLocal
            });
            entityManager.SetComponentData(entity, new PhysicsDamping()
            {
                Linear = 0.01f,
                Angular = 0.05f
            });
        }

        return entity;
    }

}
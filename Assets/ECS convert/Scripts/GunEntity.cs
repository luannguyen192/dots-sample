﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class GunEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    // implement IConvertGameObjectToEntity
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        GunComponent gun = new GunComponent()
        {
            shoot = true
        };
        dstManager.AddComponentData(entity, gun);
    }

}

public struct GunComponent : IComponentData
{
    public bool shoot;
}

public class GunSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref GunComponent gun,
            ref Translation pos) =>
        {
            if (Input.GetButton("Fire1"))
            {
                Debug.Log("Fire1");

            }
        });





    }
}